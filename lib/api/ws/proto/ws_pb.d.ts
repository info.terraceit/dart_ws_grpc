import * as jspb from 'google-protobuf'



export class Message extends jspb.Message {
  getTopic(): Topic;
  setTopic(value: Topic): Message;

  getChannelId(): string;
  setChannelId(value: string): Message;

  getMsgId(): string;
  setMsgId(value: string): Message;

  getPayload(): Uint8Array | string;
  getPayload_asU8(): Uint8Array;
  getPayload_asB64(): string;
  setPayload(value: Uint8Array | string): Message;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Message.AsObject;
  static toObject(includeInstance: boolean, msg: Message): Message.AsObject;
  static serializeBinaryToWriter(message: Message, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Message;
  static deserializeBinaryFromReader(message: Message, reader: jspb.BinaryReader): Message;
}

export namespace Message {
  export type AsObject = {
    topic: Topic,
    channelId: string,
    msgId: string,
    payload: Uint8Array | string,
  }
}

export class Broadcast extends jspb.Message {
  getIdsList(): Array<string>;
  setIdsList(value: Array<string>): Broadcast;
  clearIdsList(): Broadcast;
  addIds(value: string, index?: number): Broadcast;

  getMsg(): Message | undefined;
  setMsg(value?: Message): Broadcast;
  hasMsg(): boolean;
  clearMsg(): Broadcast;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Broadcast.AsObject;
  static toObject(includeInstance: boolean, msg: Broadcast): Broadcast.AsObject;
  static serializeBinaryToWriter(message: Broadcast, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Broadcast;
  static deserializeBinaryFromReader(message: Broadcast, reader: jspb.BinaryReader): Broadcast;
}

export namespace Broadcast {
  export type AsObject = {
    idsList: Array<string>,
    msg?: Message.AsObject,
  }
}

export class Order extends jspb.Message {
  getUserType(): number;
  setUserType(value: number): Order;

  getMsg(): Message | undefined;
  setMsg(value?: Message): Order;
  hasMsg(): boolean;
  clearMsg(): Order;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Order.AsObject;
  static toObject(includeInstance: boolean, msg: Order): Order.AsObject;
  static serializeBinaryToWriter(message: Order, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Order;
  static deserializeBinaryFromReader(message: Order, reader: jspb.BinaryReader): Order;
}

export namespace Order {
  export type AsObject = {
    userType: number,
    msg?: Message.AsObject,
  }
}

export enum Topic { 
  UNSPECIFIED = 0,
  PING = 1,
  PONG = 2,
  ACK = 3,
  RECONNECT = 4,
  NEW_CONNECTION_ESTABLISHED = 5,
  CONNECTION_CLOSED = 6,
  BROADCAST = 7,
  ORDER = 8,
}
export enum QueueGroup { 
  QUEUE_GROUP_UNSPECIFIED = 0,
  QUEUE_ORDER = 1,
}
