genDart() {
    protoc -I. -I../ \
    -I $GOBIN/include/google/protobuf \
    -I $GOBIN/include/validate \
    -I $GOPATH/src/github.com/grpc-ecosystem/grpc-gateway \
    -I $GOPATH/src/github.com/googleapis \
    --js_out=import_style=commonjs,binary:. --grpc-web_out=import_style=typescript,mode=grpcweb:. \
    --dart_out="grpc:." \
    $1/proto/$1.proto
}

########################################
services=( ws identity profile payment wallet )

if [ "$1" == "" ]; then
    for value in "${services[@]}"
    do
        echo "......proto gen $value......"
        genDart $value
    done           
else
    echo "......proto gen $1......"
    genDart $1
fi