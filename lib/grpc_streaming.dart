library grpc_streaming;

export 'package:dart_ws_grpc/api/ws/proto/ws.pbgrpc.dart' show WsClient;
export 'package:dart_ws_grpc/api/ws/proto/ws.pbenum.dart' show Topic;

import 'dart:async';
import 'dart:convert';

import 'package:dart_ws_grpc/api/ws/proto/ws.pb.dart';
import 'package:dart_ws_grpc/grpc_streaming.dart';
import 'package:grpc/grpc_web.dart';

class Gstream {
  // Singleton
  Gstream._();
  static final Gstream _instance = Gstream._();
  static Gstream getInstance() {
    return _instance;
  }

  // Properties
  late GrpcWebClientChannel channel;
  late WsClient socket;
  late String url = ""; // 'http://localhost:80'
  late String token = "";

  late StreamController<Message> controller =
      StreamController<Message>.broadcast();

  Future<void> connect(String url, String token) async {
    channel = GrpcWebClientChannel.xhr(Uri.parse(url));
    socket = WsClient(channel);

    Map<String, String> metaData = {"Authorization": "Bearer $token"};
    WebCallOptions callOptions = WebCallOptions(
        bypassCorsPreflight: true, withCredentials: false, metadata: metaData);

    controller.add(Message());
    Message ping = Message(payload: utf8.encode("Pong"));
    Stream<Message> stream = Stream<Message>.value(ping);

    final streaming = socket.streaming(stream, options: callOptions);
    streaming.listen((response) {
      print("...listening... $response");
      controller.add(response);
    }, onError: (error) {
      print("...error... $error");
      controller.addError(error);
    }, onDone: () {
      print('Closed connection to server.');
      controller.close();
    });
  }

  send(String message) {
    print('...send... $message');
    //Message msg = Message(payload: utf8.encode(message));
  }
}
