//
//  Generated code. Do not modify.
//  source: ws/proto/ws.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Topic extends $pb.ProtobufEnum {
  static const Topic UNSPECIFIED = Topic._(0, _omitEnumNames ? '' : 'UNSPECIFIED');
  static const Topic PING = Topic._(1, _omitEnumNames ? '' : 'PING');
  static const Topic PONG = Topic._(2, _omitEnumNames ? '' : 'PONG');
  static const Topic ACK = Topic._(3, _omitEnumNames ? '' : 'ACK');
  static const Topic RECONNECT = Topic._(4, _omitEnumNames ? '' : 'RECONNECT');
  static const Topic NEW_CONNECTION_ESTABLISHED = Topic._(5, _omitEnumNames ? '' : 'NEW_CONNECTION_ESTABLISHED');
  static const Topic CONNECTION_CLOSED = Topic._(6, _omitEnumNames ? '' : 'CONNECTION_CLOSED');
  static const Topic BROADCAST = Topic._(7, _omitEnumNames ? '' : 'BROADCAST');
  static const Topic ORDER = Topic._(8, _omitEnumNames ? '' : 'ORDER');

  static const $core.List<Topic> values = <Topic> [
    UNSPECIFIED,
    PING,
    PONG,
    ACK,
    RECONNECT,
    NEW_CONNECTION_ESTABLISHED,
    CONNECTION_CLOSED,
    BROADCAST,
    ORDER,
  ];

  static final $core.Map<$core.int, Topic> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Topic? valueOf($core.int value) => _byValue[value];

  const Topic._($core.int v, $core.String n) : super(v, n);
}

class QueueGroup extends $pb.ProtobufEnum {
  static const QueueGroup QUEUE_GROUP_UNSPECIFIED = QueueGroup._(0, _omitEnumNames ? '' : 'QUEUE_GROUP_UNSPECIFIED');
  static const QueueGroup QUEUE_ORDER = QueueGroup._(1, _omitEnumNames ? '' : 'QUEUE_ORDER');

  static const $core.List<QueueGroup> values = <QueueGroup> [
    QUEUE_GROUP_UNSPECIFIED,
    QUEUE_ORDER,
  ];

  static final $core.Map<$core.int, QueueGroup> _byValue = $pb.ProtobufEnum.initByValue(values);
  static QueueGroup? valueOf($core.int value) => _byValue[value];

  const QueueGroup._($core.int v, $core.String n) : super(v, n);
}


const _omitEnumNames = $core.bool.fromEnvironment('protobuf.omit_enum_names');
