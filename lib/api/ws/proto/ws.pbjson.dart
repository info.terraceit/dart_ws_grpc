//
//  Generated code. Do not modify.
//  source: ws/proto/ws.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use topicDescriptor instead')
const Topic$json = {
  '1': 'Topic',
  '2': [
    {'1': 'UNSPECIFIED', '2': 0},
    {'1': 'PING', '2': 1},
    {'1': 'PONG', '2': 2},
    {'1': 'ACK', '2': 3},
    {'1': 'RECONNECT', '2': 4},
    {'1': 'NEW_CONNECTION_ESTABLISHED', '2': 5},
    {'1': 'CONNECTION_CLOSED', '2': 6},
    {'1': 'BROADCAST', '2': 7},
    {'1': 'ORDER', '2': 8},
  ],
};

/// Descriptor for `Topic`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List topicDescriptor = $convert.base64Decode(
    'CgVUb3BpYxIPCgtVTlNQRUNJRklFRBAAEggKBFBJTkcQARIICgRQT05HEAISBwoDQUNLEAMSDQ'
    'oJUkVDT05ORUNUEAQSHgoaTkVXX0NPTk5FQ1RJT05fRVNUQUJMSVNIRUQQBRIVChFDT05ORUNU'
    'SU9OX0NMT1NFRBAGEg0KCUJST0FEQ0FTVBAHEgkKBU9SREVSEAg=');

@$core.Deprecated('Use queueGroupDescriptor instead')
const QueueGroup$json = {
  '1': 'QueueGroup',
  '2': [
    {'1': 'QUEUE_GROUP_UNSPECIFIED', '2': 0},
    {'1': 'QUEUE_ORDER', '2': 1},
  ],
};

/// Descriptor for `QueueGroup`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List queueGroupDescriptor = $convert.base64Decode(
    'CgpRdWV1ZUdyb3VwEhsKF1FVRVVFX0dST1VQX1VOU1BFQ0lGSUVEEAASDwoLUVVFVUVfT1JERV'
    'IQAQ==');

@$core.Deprecated('Use messageDescriptor instead')
const Message$json = {
  '1': 'Message',
  '2': [
    {'1': 'topic', '3': 1, '4': 1, '5': 14, '6': '.ws.Topic', '10': 'topic'},
    {'1': 'channel_id', '3': 2, '4': 1, '5': 9, '10': 'channelId'},
    {'1': 'msg_id', '3': 3, '4': 1, '5': 9, '10': 'msgId'},
    {'1': 'payload', '3': 4, '4': 1, '5': 12, '10': 'payload'},
  ],
};

/// Descriptor for `Message`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageDescriptor = $convert.base64Decode(
    'CgdNZXNzYWdlEh8KBXRvcGljGAEgASgOMgkud3MuVG9waWNSBXRvcGljEh0KCmNoYW5uZWxfaW'
    'QYAiABKAlSCWNoYW5uZWxJZBIVCgZtc2dfaWQYAyABKAlSBW1zZ0lkEhgKB3BheWxvYWQYBCAB'
    'KAxSB3BheWxvYWQ=');

@$core.Deprecated('Use broadcastDescriptor instead')
const Broadcast$json = {
  '1': 'Broadcast',
  '2': [
    {'1': 'ids', '3': 1, '4': 3, '5': 9, '10': 'ids'},
    {'1': 'msg', '3': 2, '4': 1, '5': 11, '6': '.ws.Message', '10': 'msg'},
  ],
};

/// Descriptor for `Broadcast`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List broadcastDescriptor = $convert.base64Decode(
    'CglCcm9hZGNhc3QSEAoDaWRzGAEgAygJUgNpZHMSHQoDbXNnGAIgASgLMgsud3MuTWVzc2FnZV'
    'IDbXNn');

@$core.Deprecated('Use orderDescriptor instead')
const Order$json = {
  '1': 'Order',
  '2': [
    {'1': 'user_type', '3': 1, '4': 1, '5': 5, '10': 'userType'},
    {'1': 'msg', '3': 2, '4': 1, '5': 11, '6': '.ws.Message', '10': 'msg'},
  ],
};

/// Descriptor for `Order`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List orderDescriptor = $convert.base64Decode(
    'CgVPcmRlchIbCgl1c2VyX3R5cGUYASABKAVSCHVzZXJUeXBlEh0KA21zZxgCIAEoCzILLndzLk'
    '1lc3NhZ2VSA21zZw==');

