//
//  Generated code. Do not modify.
//  source: ws/proto/ws.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:async' as $async;
import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'package:protobuf/protobuf.dart' as $pb;

import 'ws.pb.dart' as $0;

export 'ws.pb.dart';

@$pb.GrpcServiceName('ws.Ws')
class WsClient extends $grpc.Client {
  static final _$streaming = $grpc.ClientMethod<$0.Message, $0.Message>(
      '/ws.Ws/Streaming',
      ($0.Message value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Message.fromBuffer(value));

  WsClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options,
        interceptors: interceptors);

  $grpc.ResponseStream<$0.Message> streaming($async.Stream<$0.Message> request, {$grpc.CallOptions? options}) {
    return $createStreamingCall(_$streaming, request, options: options);
  }
}

@$pb.GrpcServiceName('ws.Ws')
abstract class WsServiceBase extends $grpc.Service {
  $core.String get $name => 'ws.Ws';

  WsServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Message, $0.Message>(
        'Streaming',
        streaming,
        true,
        true,
        ($core.List<$core.int> value) => $0.Message.fromBuffer(value),
        ($0.Message value) => value.writeToBuffer()));
  }

  $async.Stream<$0.Message> streaming($grpc.ServiceCall call, $async.Stream<$0.Message> request);
}
