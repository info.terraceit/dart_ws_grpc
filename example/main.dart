import 'dart:core';
import 'dart:convert';
import 'package:grpc/grpc_web.dart';
import 'package:dart_ws_grpc/api/ws/proto/ws.pb.dart';
import 'package:dart_ws_grpc/grpc_streaming.dart';

void main() async {
  String url = 'http://localhost:80';
  String token =
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTcyMTA0NDYsImlhdCI6MTcxNzEyNDA0NiwiaXNzIjoidGVzdGluZ0BzZWN1cmUuaXN0aW8uaW8iLCJqdGkiOiIyOWMzNzQ0YS0yMTEzLTQ1MGQtYjYzNC1jNjExYzk4ZGZmMTciLCJwZXJtaXNzaW9uIjoicnciLCJyb2xlIjoiT1JERVIiLCJzdWIiOiJ0ZXN0aW5nIiwidHlwZSI6Mn0.TSWcLMZUJ4jEIIqksTjJjDVGNJ0vQVZAJVAv2kqtXHsmg7ClPyPS9NUf-znGnmc579JdJbY7TUp1zOScrxg33I_dp3OD5y7XDNR1c980O8tFjMeVf-5xWevYRgxV-67-FFFsyw7fJmoWoAmu62RgKUUKalGpbLM0qdY61xKozdN3zMr6VtugKkJPuFXUHKCnxNGKyJtsEmTE4RqmqzG7pcSCT-56qWQ8edVKCOs0zK-MUzA9F9xjZVfKowVRspWSybP9HRba24QTQTxMNcVc_BwpBg3GMd1nYhYRGiOulnstdLruUbCs7uV61uhmuCQcrkiZljo36f_QoLDpwsB3Kg';

  Gstream.getInstance().connect(url, token);

  await Future<void>.delayed(const Duration(seconds: 3));

  Gstream.getInstance().send('hello ws grpc web');

  while (true) {}
  ;
}
